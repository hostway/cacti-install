#!/bin/bash
yum -y install cacti
echo -n "MySQL Password:"
read -s password
mysql -u root -p$password -e ";"
if [ $? -ne 0 ]; then
  echo "Password 정보가 다릅니다. 다시 시도해주세요."
  exit 0
fi

mysql -u root -p$password cacti < /usr/share/doc/cacti-*/cacti.sql

mv /usr/share/cacti/include/config.php /usr/share/cacti/include/config.php.bak
sed -e '31s/cactiuser/]jbUHbsTa@W5Z4!/g' /usr/share/cacti/include/config.php.bak > /usr/share/cacti/include/config.php

echo "*/5 * * * *    cacti   /usr/bin/php /usr/share/cacti/poller.php > /dev/null 2>&1" > /etc/cron.d/cacti

mv /etc/httpd/conf.d/cacti.conf /etc/httpd/conf.d/cacti.conf.ori
sed -e "s/Require host localhost/Require all granted/g" /etc/httpd/conf.d/cacti.conf.ori > /etc/httpd/conf.d/cacti.conf

mv /etc/php.ini /etc/php.ini.ori
sed -e "s/;date.timezone =/date.timezone = Asia\/Seoul/g" /etc/php.ini.ori > /etc/php.ini

systemctl restart httpd
systemctl restart snmpd
systemctl restart mariadb

echo "Cacti 설치 완료. 웹 브라우저에서 \"http://THIS_SERVER_IPADDRESS/cacti/\"로 접속하여 나머지 설치를 진행하세요."
