#!/bin/bash
mysql_secure_installation

echo -n "MySQL Password:"
read -s password
mysql -u root -p$password -e ";"
if [ $? -ne 0 ]; then
  echo "Password 정보가 다릅니다. 다시 시도해주세요."
  exit 0
fi

mysql -u root -p$password mysql < /usr/share/mysql/mysql_test_data_timezone.sql
mysql -u root -p$password -e "create database cacti;"
mysql -u root -p$password -e "GRANT ALL ON cacti.* TO cactiuser@localhost IDENTIFIED BY ']jbUHbsTa@W5Z4!';"
mysql -u root -p$password -e "GRANT SELECT ON mysql.time_zone_name TO cactiuser@localhost;"
mysql -u root -p$password -e "flush privileges;"
echo
mysql -u root -p$password -e "show databases;"

cat << "EOF" > /etc/my.cnf.d/server.cnf
[mysqld]
collation-server = utf8_general_ci
init-connect='SET NAMES utf8'
character-set-server = utf8
max_heap_table_size = 128M
max_allowed_packet = 16777216
tmp_table_size = 64M
join_buffer_size = 64M
innodb_file_per_table = on
innodb_buffer_pool_size = 512M
innodb_doublewrite = off
innodb_additional_mem_pool_size = 80M
innodb_lock_wait_timeout = 50
innodb_flush_log_at_trx_commit = 2
EOF
