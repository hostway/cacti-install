#!/bin/bash
rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
yum -y install net-snmp net-snmp-utils net-snmp-libs rrdtool
yum -y install mariadb-server php php-xml php-session php-sockets php-ldap php-gd
systemctl start httpd
systemctl start snmpd
systemctl start mariadb
